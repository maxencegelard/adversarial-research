from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf.urls.static import static

urlpatterns = [
        path('', views.accueil),
        path('tictactoe', views.titactoe),
        path('tictactoe/minmaxexp', views.minmaxexplanation),
        path('tictactoe/treeexp', views.treeexplanation),
        path('pruning', views.pruning),
        path('connectfour', views.connectfour),
        path('chess', views.chess),
        path('about', views.about),
        path('register', views.register),
        path('signin', views.signin, name='signin'),
        path('logout', views.logout, name='logout'),
        path('colab', views.colab),
]


