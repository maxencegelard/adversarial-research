from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from .forms import ConnectionForm
from django.urls import reverse
from django.contrib.auth import logout as django_logout
from django.contrib.auth.decorators import login_required

# Create your views here


def accueil(request):
    return render(request, 'blog/accueil.html', locals())

@login_required
def titactoe(request):
    return render(request, 'blog/tictactoe.html', locals())

@login_required
def minmaxexplanation(request):
    return render(request, 'blog/minmaxexplanation.html', locals())

@login_required
def treeexplanation(request):
    return render(request, 'blog/treeexplanation.html', locals())

@login_required
def pruning(request):
    return render(request, 'blog/pruning.html', locals())

@login_required
def connectfour(request):
    return render(request, 'blog/connectfour.html', locals())

@login_required
def chess(request):
    return render(request, 'blog/chess.html', locals())

@login_required
def colab(request):
    return redirect("https://colab.research.google.com/drive/1ZbhX9AQEm8VcVvnzJnGdg2CC5NFknzoi#scrollTo=CM06eTm2joRq")

def about(request):
    return render(request, 'blog/about.html', locals())

def register(request):
    
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request, 'blog/accueil.html', locals())
    else:
        form = UserCreationForm()

    return render(request, 'blog/register.html', locals())

def signin(request):
    error = False
    
    if request.method == 'POST':
        form = ConnectionForm(request.POST)
        
        if form.is_valid():
            #form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            
            if user:
                login(request, user)
                
            else:
                error = True
    else:
        form = ConnectionForm()

    return render(request, 'blog/signin.html', locals())

def logout(request):
    django_logout(request)
    return redirect(reverse(signin))

