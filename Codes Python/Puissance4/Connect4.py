class ConnectFour:


    def __init__(self):

        self.title = "Puissance 4"
        self.lines = int(6)
        self.rows = int(7)
        self.number_of_winning_alignment = int(4)
        self.number_players = int(2)
        self.empty = 0
        self.red = 1
        self.yellow = 2
        self.color_symbol = ("_", "X", "O") #X pour red et O pour yellow
        self.heuristic_table = [[3, 4, 5, 7, 5, 4, 3], [4, 6, 8, 10, 8, 6, 4], [5, 8, 11, 13, 11, 8, 5], [5, 8, 11, 13, 11, 8, 5], [4, 6, 8, 10, 8, 6, 4], [3, 4, 5, 7, 5, 4, 3]]

        self.grid = []
        for line in range(8) :
             self.grid.append(list((int(9)) * [0])) # pareil



    def is_valid(self, row):

        # en dehors de la grid
        if row < 1 or row > self.rows:
            return False
        else: #renvoie True si on peut mettre un nouveau jeton dans la row
            return self.grid[self.lines][row] == 0





    def available(self):
        #Fonction qui permet de déterminer dans quelles colonnes on peut jouer
        L=[]
        for row in range(1,8):
            if self.is_valid(row):
                L.append(row)
        return L # Renvoie la liste des colonnes disponibles pour le prochain coup


    def fall_line(self, row):

        line = 1
        while self.grid[line][row] != 0:
            line = line + 1
        # renvoie la ligne sur laquelle se place le jeton en tombant
        return line

    def drop(self, row, color):
        # met à jour la valeur de la case
        self.grid[self.fall_line(row)][row] = color

    def play(self, row, isMaxPlayer):

        if isMaxPlayer:
            color = 1
        else:
            color = 2

        self.drop(row, color)

    def symbol_move(self, color) : #renvoie _ ou X ou O
        return self.color_symbol[color]

    def display_rows_numbers(self): # affiche 1 2 3 4 5 6 7
        j = int()
        print("_",end="_")
        for j in range(1, self.rows + 1) : print(j, end="_")
        print()

    def display(self):
        i = int() ; j = int()
        # on affiche le title
        print()
        print("__", self.title)
        # on affiche le bord supérieur
        self.display_rows_numbers()
        # on affiche la grid
        for i in range(self.lines, 0, -1):
        # on affiche le numéro de ligne à gauche
            print(i,end="_")
        # on affiche la ligne i
            for j in range(1, self.rows + 1):
                print(self.symbol_move(self.grid[i][j]), end="_")
        # on affiche le numéro de ligne à droite
            print(i)
        # on affiche le bord inférieur
        self.display_rows_numbers()



    def number_paws_direction(self, inLine, inRow, inDirX, inDirY):
        line = int()
        row = int()
        color = int()
        numberPawns = int()
        # initialisation
        color = self.grid[inLine][inRow]
        numberPawns = 1
        # compter les jetons dans la direction inDirX,inDirY
        line = inLine + inDirY
        row = inRow + inDirX
        while self.grid[line][row] == color:
            numberPawns = numberPawns + 1
            line = line + inDirY
            row= row + inDirX
        # compter les jetons dans la direction opposée
        line = inLine - inDirY
        row = inRow - inDirX
        while self.grid[line][row] == color:
            numberPawns = numberPawns + 1
            line = line - inDirY
            row= row - inDirX
        # renvoi du résultat
        return numberPawns

    def max(inEntier1, inEntier2): #on recode la fonction max au cas où
        if inEntier1 > inEntier2: return inEntier1
        else: return inEntier2

    def number_aligned_pawns(self, inLine, inRow): # calcule pour une position donnée le nombre de pions alignés
        numberPawns= int(1)
        if self.grid[inLine][inRow] != 0:
            # test dans toutes les directions
            numberPawns = max(numberPawns, self.number_paws_direction(inLine, inRow, 1, 1))
            numberPawns = max(numberPawns, self.number_paws_direction(inLine, inRow, 1, 0))
            numberPawns = max(numberPawns, self.number_paws_direction(inLine, inRow, 1, -1))
            numberPawns = max(numberPawns, self.number_paws_direction(inLine, inRow, 0, 1))
        return numberPawns

    def is_player(self): #Fonction qui sert à identifier le joueur
        j1 = 0
        j2 = 0
        for i in range(1, self.lines + 1):
            for j in range(1, self.rows + 1):
                if self.grid[i][j] == 1:
                    j1 += 1
                if self.grid[i][j] == 2:
                    j2 += 1
        if j1 > j2: # Si le nombre de pièces du joueur 1 est supérieur à celle du joueur 2, c'est au joueur 2
            return False
        if j1 == j2: # Sinon, c'est au joueur 1
            return True

    def winning(self,isMaxPlayer): #Fonction qui test pour un joueur donné si il a gagné ou non
        a = 0
        if isMaxPlayer:
            couleur = 1
        else:
            couleur = 2
        for i in range(1, self.lines + 1):
            for j in range(1, self.rows + 1):
                if self.grid[i][j] == couleur :
                    a = max(a, self.number_aligned_pawns(i, j))
        if a >= 4:
            return 1 #gagne renvoie donc un booléen
        if a < 4:
            return 0

    def end_game(self): #test si le jeu est terminé ou non
        if self.winning(self.is_player()):
            return True
        return False

    def cancel(self, row): # permet d'annuler le dernier coup joué sur la row ciblé
        rank = 1
        while self.grid[rank][row] != 0:
            rank += 1
        rank = rank - 1
        self.grid[rank][row] = 0
        
    def total_number_pawns(self):
        n = 0
        for i in range(1, self.lines + 1):
            for j in range(1, self.rows + 1):
                if self.grid[i][j] != 0:
                    n += 1
        return n

    def heuristic(self): #permet d'estimer la valeur d'une instance
        if self.winning(True): #on test si un des joueurs gagne et on y met un poid conséquent si c'est le cas
            return 100*(42 - self.total_number_pawns())
        elif self.winning(False):
            return -100*(42 - self.total_number_pawns())
        else: # si personne ne gagne, on estime l'instance en utilisant le tableau d'heuristique
            v1 = 0
            v2 = 0
            if not(self.is_player()):
                color = 1
            else:
                color = 2
            for i in range(1, self.lines + 1):
                for j in range(1, self.rows + 1):
                    if self.grid[i][j] == 1:
                        v1 += self.heuristic_table[i - 1][j - 1]
                    if self.grid[i][j] == 2:
                        v2 += self.heuristic_table[i - 1][j - 1]
            if color == 1:
                return v1-v2
            if color == 2:
                return v2-v1
            

