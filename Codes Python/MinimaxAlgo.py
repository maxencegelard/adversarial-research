from math import inf as infinity


def minimax(jeu, depth, isMaxPlayer):
    """
    AI function that choice the best move
    :param jeu : TTT / ConnectFour / Chess
    :param depth: node index
    :param isMaxplayer: an human ( = True ) or a computer ( = False)
    :return: a list with [the best move, best score]
    """
    if isMaxPlayer:
        best = [-1, -infinity]
    elif not isMaxPlayer:
        best = [-1, +infinity]

    if depth == 0 or jeu.end_game():
        score = jeu.heuristic()
        return [-1, score]


    for cell in jeu.available():
        jeu.play(cell, isMaxPlayer)
        score = minimax(jeu, depth - 1,not isMaxPlayer)
        jeu.cancel(cell)
        score[0] = cell

        if isMaxPlayer:
            if score[1] > best[1]:
                best = score  # max value
        elif not isMaxPlayer:
            if score[1] < best[1]:
                best = score  # min value

    return best
