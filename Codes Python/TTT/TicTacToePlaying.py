import math
import random
from TTT.TicTacToe import *
from MinimaxAlgo import *
import time
from AlphaBetaPruning import *



def AI_turn(game):


    depth = len(game.available())

    if depth == 9:
        move = random.choice([0,1,2,3,4,5,6,7,8])
        game.play(move, False)
    else:

        best = minimax(game, depth, False)
        move = best[0]
        game.play(move, False)

    print('o' + " plays at " + str (move))


def Human_turn(game):

    profondeur = len(game.available())

    # Choice of the move
    valid = False
    while valid != True:
        move = int (input ("where are you playing ? "))
        for k in range(len(game.available())):
            if move == game.available()[k]:
                move = game.available()[k]

                valid = True
                break
    game.play(move, True)
    print('x' + " plays at " + str (move))

def IAOpponent(game):

    depth = len(game.available())

    if depth == 9:
        move = random.choice([0,1,2,3,4,5,6,7,8])
        game.play(move, True)
    else:

        best = minimax(game, depth, True)
        move = best[0]
        game.play(move, True)

    print('x' + " plays at " + str (move))




def TicTacToeGame():

    game = TicTacToe()

    turn = input("Who begins ? x OR o")

    while not game.end_game():

        if turn == 'x':
            Human_turn(game)
            game.display()

            if game.winning(True):
                print("x wins")
                break

            if game.tie():
                print("Tie !")
                break

            turn = 'o'

        else:
            AI_turn(game)
            game.display()

            if game.winning(False):
                print("o wins")
                break
            if game.tie():
                print("Tie !")
                break

            turn = 'x'


    return 0

def IAvsIA():

    game = TicTacToe()



    while not game.end_game():


        IAOpponent(game)
        game.display()

        if game.winning(True):
            print("x wins")
            break

        if game.tie():
            print("Tie !")
            break



        AI_turn(game)
        game.display()

        if game.winning(False):
            print("o wins")
            break

        if game.tie():
            print("Tie !")
            break



#TicTacToeGame()
#IAvsIA()


def AB_HuTurn(game, alpha, beta):

    depth = len(game.available())

    # Choix de la case
    valid = False
    while valid != True:
        case = int (input ("where are you playing ? "))
        for k in range(len(game.available())):
            if case == game.available()[k]:
                case = game.available()[k]

                valid = True
                break
    game.play(case, True)
    print('x' + " plays at " + str (case))

def AB_IATurn(game, alpha, beta):

    depth = len(game.available())

    if depth == 9:
        move = random.choice([0,1,2,3,4,5,6,7,8])
        game.play(move, False)
    else:

        best = alphaBetaPruning(game, alpha, beta, depth, False)
        move = best[0]
        game.play(move, False)

    print('o' + " joue en " + str (move))

def AB_IAOpponenTurn(game, alpha, beta):

    depth = len(game.available())

    if depth == 10:
        move = random.choice([0,1,2,3,4,5,6,7,8])
        game.play(move, True)
    else:

        best = alphaBetaPruning(game, alpha, beta, depth, True)
        move = best[0]
        game.play(move, True)

    print('x' + " plays at " + str (move))




def TTTPlayingAB():

    game = TicTacToe()
    alpha = - math.inf
    beta = math.inf
    turn = input("Who begins ? x OR o")

    while not game.end_game():

        if turn == 'x':
            AB_HuTurn(game,alpha,beta)
            game.display()

            if game.winning(True):
                print("x wins")
                break

            if game.tie():
                print("Tie !")
                break

            turn = 'o'

        else:
            AB_IATurn(game,alpha,beta)
            game.display()

            if game.winning(False):
                print("o wins")
                break
            if game.tie():
                print("Tie !")
                break

            turn = 'x'

TTTPlayingAB()

def AB_AIvsIA():

    game = TicTacToe()
    alpha = - math.inf
    beta = math.inf


    while not game.end_game():


        AB_IAOpponenTurn(game,alpha,beta)
        game.display()

        if game.winning(True):
            print("x wins")
            break

        if game.tie():
            print("Tie !")
            break



        AB_IATurn(game,alpha,beta)
        game.display()

        if game.winning(False):
            print("o wins")
            break

        if game.tie():
            print("Tie !")
            break

#AB_AIvsIA()


