import math
import random


class TicTacToe():

    def __init__(self):

        self.grid = [' '] * 9

    def available(self):

        available = []
        for i in range(len(self.grid)):
            if self.grid[i] == ' ':
                available.append(i)
        return available

    def is_valid(self, move):
        """

        :param move: int between 1 and 9
        :param player: string 'x' or 'o'
        :return:
        """

        if move in self.available():
            return True
        else:
            return False

    def play(self, move, isMaxPlayer):
        """

        :param move: int between 1 and 9
        :param player: string 'x' or 'o'
        :return:
        """

        #if isMaxPlayer:
        #    player = 'x'
        #elif not isMaxPlayer:
        #    player = 'o'

        if isMaxPlayer:
            symbol = 'x'
        elif not isMaxPlayer:
            symbol = 'o'

        if self.is_valid(move):
            self.grid[move] = symbol

    def cancel(self, move):

        self.grid[move] = ' '

    def winning(self,isMaxPlayer):

        if isMaxPlayer:
            player = 'x'
        elif not isMaxPlayer:
            player = 'o'

        board = self.grid

        if(
        (board[0] == player and board[1] == player and board[2] == player) or
        (board[3] == player and board[4] == player and board[5] == player) or
        (board[6] == player and board[7] == player and board[8] == player) or
        (board[0] == player and board[3] == player and board[6] == player) or
        (board[1] == player and board[4] == player and board[7] == player) or
        (board[2] == player and board[5] == player and board[8] == player) or
        (board[0] == player and board[4] == player and board[8] == player) or
        (board[2] == player and board[4] == player and board[6] == player)
        ):
            return True
        else:
            return False

    def end_game(self):

        if self.winning(True) or self.winning(False):

            return True
        return False

    def display(self):

        grille = self.grid

        print("     0)  1)  2)")
        print("   -------------")
        print("0)", end='')
        for i in range(3):
            print(" | "+str(grille[i]), end='')
        print(" |")
        print("   -------------")
        print("1)", end='')
        for i in range(3):
            print(" | "+str(grille[i+3]), end='')
        print(" |")
        print("   -------------")
        print("2)", end='')
        for i in range(3):
            print(" | "+str(grille[i+6]), end='')
        print(" |")
        print("   -------------")

    def heuristic(self):
        if self.winning(True):
            return 1
        elif self.winning(False):
            return -1
        else:
            return 0

    def tie(self):

        if len(self.available()) == 0:
            return True
        return False





        
            
    
