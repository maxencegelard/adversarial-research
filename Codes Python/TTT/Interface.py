
from MinimaxAlgo import *
from TTT.TicTacToe import *
from tkinter import *
from tkinter import messagebox
import networkx as nx
import matplotlib.pyplot as plt
import math
import warnings
warnings.filterwarnings("ignore")

# Execute this code if you already went through "TicTacToePlaying"
# It is a simple interface to play TTT

game = TicTacToe()

player = True
ia = False
Tree_Game = nx.Graph()
depth = 0
liste_centres = [(50,50),(150,50),(250,50),(50,150),(150,150),(250,150),(50,250),(150,250),(250,250)]


def dist(A,B):
    return math.sqrt((B[0]-A[0])**2+(B[1]-A[1])**2)

def left_click(event):
    plt.close()


    centre = liste_centres[0]
    point = (event.x,event.y)
    min = dist(centre,point)
    place = 0
    for i in range(len(liste_centres)):
        if dist(point,liste_centres[i]) < min:
            centre = liste_centres[i]
            min = dist(centre,point)
            place = i

    croix = Button(canvas, width = 80, height = 80, image=photo)
    croix_w = canvas.create_window(centre[0], centre[1], window=croix)


    game.play(place, player)
    Tree_Game.add_node('x'+str(place))
    if len(Tree_Game.nodes) > 1:
            Tree_Game.add_edge(list(Tree_Game.nodes)[len(Tree_Game.nodes)-2],list(Tree_Game.nodes)[len(Tree_Game.nodes)-1])



    coupsPossibles = game.available()
    if coupsPossibles != []:

        caseAjouer = minimax(game,len(coupsPossibles),False)[0]

        game.play(caseAjouer, False)

        Tree_Game.add_node('o'+str(caseAjouer))

        if len(Tree_Game.nodes) > 1:
            print(Tree_Game.nodes)
            Tree_Game.add_edge(list(Tree_Game.nodes)[len(Tree_Game.nodes)-2],list(Tree_Game.nodes)[len(Tree_Game.nodes)-1])


        rond = Button(canvas, width = 80, height = 80, image=photo2)
        rond_w = canvas.create_window(liste_centres[caseAjouer][0], liste_centres[caseAjouer][1], window=rond)



    if game.winning(player):
        messagebox.showinfo("End","You won")
    elif game.winning(ia):
        go = Button(canvas, width = 300, height = 300, image=photo3)
        go_w = canvas.create_window(450,150 , window=go)

    
    elif game.available() == []:
        messagebox.showinfo("End","Tie")
    options = {
        'node_size':500
    }
    nx.draw(Tree_Game, with_labels=True, **options)
    plt.show()



fenetre = Tk()
label = Label(fenetre, text="TicTacToe")
label.pack()

canvas = Canvas(fenetre, width=600, height=300, background='turquoise')



ligne1 = canvas.create_line(0, 100, 300, 100, width = 3, fill = "white", activefill = "purple")
ligne2 = canvas.create_line(0, 200, 300, 200, width = 3, fill = "white", activefill = "purple")
ligne3 = canvas.create_line(100, 0, 100, 300, width = 3, fill = "white", activefill = "purple")
ligne4 = canvas.create_line(200, 0, 200, 300, width = 3, fill = "white", activefill = "purple")


canvas.pack()

canvas.bind('<Button-1>',left_click)





photo = PhotoImage(file='croix.png')
photo2 = PhotoImage(file='rond.png')
photo3 = PhotoImage(file = 'gameover.png')



fenetre.mainloop()


