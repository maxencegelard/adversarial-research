from Chess.classChess import *
from MinimaxAlgo import *

# Initialisation de la partie

play = Chess()

while True:
    print(play.board) # Affichage de l'état du plateau après chaque coup (ie un déplacement du joueur, et un coup de l'ordinateur)
    legalMoves = play.board.legal_moves
    if legalMoves.count() == 0: # Dans le cas où blanc ne peut plus jouer, les noirs ont gagné
        print("End")
        break
    movePlayer = input("Next move : ")
    if chess.Move.from_uci(movePlayer) in legalMoves: # Vérification de la légalité du coup souhaité
        Chess.play(play, movePlayer)
        AIMove = minimax(play, 3, False) # Calcul du coup de l'ordinateur
        if AIMove[0] == -1:  # Dans le cas où l'ordinateur ne peut plus jouer, les blans ont gagné
            print("Congratulations")
            break
        Chess.play(play, AIMove[0]) # Déplacement de l'ordinateur
        print(chess.Move.uci(AIMove[0]) + "\n") # Affichage du code UCI du coup de l'ordinateur
    else:
        print("\nInvalid move, please input a valid move.")
