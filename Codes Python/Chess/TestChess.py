from Chess.classChess import *


def test1():  # Affichage d'un plateau de base, et de tous les coups possibles en notation UCI dans la coniguration du plateau en question.
    blank_board = Chess()
    legal_moves = Chess.available(blank_board)
    print(blank_board.board)
    for i in legal_moves:
        print(i)


def test2():  # Partie simple jouée (mat du berger), et test des fonction annuler et end_game, qui permet de savoir si la partie est terminée.
    blank_board = Chess()
    Chess.play(blank_board, 'e2e4')
    Chess.play(blank_board, 'e7e5')
    Chess.play(blank_board, 'f1c4')
    Chess.play(blank_board, 'd7d6')
    Chess.play(blank_board, 'd1f3')
    Chess.play(blank_board, 'c7c5')
    print(blank_board.board)
    print(Chess.end_game(blank_board))
    Chess.play(blank_board, 'f3f7')
    print(blank_board.board)
    print(Chess.end_game(blank_board))
    Chess.cancel(blank_board)
    print(blank_board.board)


def test3():  # Test de l'heuristique, avec une position mat en un et un position d'avantage materiel
    blank_board = Chess()
    print("Score position de départ : ", Chess.heuristic(blank_board))
    Chess.play(blank_board, 'e2e4')
    Chess.play(blank_board, 'e7e5')
    Chess.play(blank_board, 'f1c4')
    Chess.play(blank_board, 'd7d6')
    Chess.play(blank_board, 'd1f3')
    Chess.play(blank_board, 'c7c5')
    print(blank_board.board)
    print("Score position 1 : ", Chess.heuristic(blank_board))
    Chess.play(blank_board, 'f3f5')
    Chess.play(blank_board, 'c8f5')
    print(blank_board.board)
    print("Score position 2 : ", Chess.heuristic(blank_board))


test1()
test2()
test3()
